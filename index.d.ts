/** Options consumed by the SiegeGGApiClient. */
class ApiClientOptions {
    /**
     * This string is the version part of the API.
     * Check https://dev.siege.gg/docs for the current API version.
     * @default 'v1'
     */
    version?: String;
    /**
     * The API token is a secret string which is used to authenticate with the SiegeGG API.
     */
    token: String;
}

/**
 * A high-level interface to the SiegeGG API.
 * @see https://dev.siege.gg/docs for the latest API documentation.
 */
export class SiegeGGApiClient {
    constructor(options: ApiClientOptions);

    /**
     * @returns Promise<SiegeGGApiClient>
     */
    app(): Promise<ApiApplication>;

    /** Updates the application title */
    updateAppTitle(): Promise<ApiApplication>;

    /**
     * @returns BasicRequester
     */
    basic(): BasicRequester;

    /** Get a list of all seasons */
    seasons(): Promise<SeasonPage>;

    /** Get a single season by id */
    season(id): Promise<Season>;

    matches(): Promise<MatchPage>;

    match(id): Promise<Match>;

    players(): Promise<PlayerPage>;

    player(ign: String): Promise<Player>;

    /** Get all player picks */
    playerPicks(ign: String): Promise<PlayerPicks>;

    games(): Promise<GamePage>;

    game(id): Promise<Game>;

    rounds(): Promise<RoundPage>;

    round(id): Promise<Round>;

    rosters(): Promise<RosterPage>;

    roster(id): Promise<Roster>;

    orgs(): Promise<OrganizationPage>;

    org(id): Promise<Organization>;

    /**
     * Set the page for pagination endpoints.
     * @returns this
     * @example 'One-Line'
     * var seasons = api.page(4).seasons()
     * @example 'Multi-Line'
     * api.page(4)
     * var seasons = api.seasons()
     */
    page(page? = 0): SiegeGGApiClient;
}

export enum ApiApplicationType {
    community = 'community',
    staff = 'staff',
    enterprise = 'enterprise',
}

class DateTime {
    /**
     * `created_at` is the date when the current object was created or added to the database.
     */
    created_at: Date;

    /**
     * `updated_at` is the date when the current object was last updated.
     * However, this field may not be accurate at all times because basic manipulations
     * which were made automatically, like the `qouta_reset` mechanism, will not interfer
     * with this field. If the current object is mutable by an API application it is very
     * likely that the field indicates the last change by that application.
     */
    updated_at: Date;
}

/**
 * An API application of the SiegeGG API.
 * @see https://dev.siege.gg
 */
class ApiApplication extends DateTime {
    /** The title of the application. */
    title: String;

    /** The owner of the application. */
    owner: String;

    /** All scopes of the application. */
    scopes: Array<String>;

    /**
     * The type of the application.
     * One of: `community`, `staff` or `enterprise`
     */
    type: ApiApplicationType;

    /** The remaining requests until `quota_reset`. */
    quota_remaining: Number;

    /** When `qouta_reset` > `Date.now()` then `qouta_reset` will be reset. */
    qouta_reset: Date;

    constructor(data: Array<Object>);
}

class Pagination {
    /** The current page */
    page: Number;
    /** The total count of objects */
    count: Number;
    /** The number of total pages */
    total: Number;
}

class Page {
    /** The data of the page */
    data: Array<any>;
    pagination: Pagination;
}

/**
 * This does not include all the languages of the world but all
 * article languages to expect (for now).
 */
export enum ArticleLanguage {
    /* English/English */
    en = 'en',

    /** German/Deutsch */
    de = 'de',

    /** Dutch/Nederlands */
    nl = 'nl',

    /** Slovenian/Slovinčina */
    sl = 'sl',

    /** Portuguese/Português */
    pt = 'pt',

    /** Italian/Italiano */
    it = 'it',

    /** Chinese, Mandarin */
    cmn = 'cmn',

    /** Japanese */
    jp = 'jp',

    /** Spanish/Español */
    es = 'es',

    /** French/Français */
    fr = 'fr',

    /** Hebrew */
    he = 'he',
}

/** A list of basic news articles. */
class BasicArticlePage extends Page {
    /** @override */
    data: Array<BasicArticle>;
    /** The pagination is null because there only exists one BasicArticlePage  */
    pagination: null;
}

class SeasonPage extends Page {
    data: Array<Season>;
}

class MatchPage extends Page {
    data: Array<Match>;
}

class PlayerPage extends Page {
    data: Array<Player>;
}

class PlayerPicks extends Page {
    data: Array<OperatorPick>;
}

class GamePage extends Page {
    data: Array<Game>;
}

class RoundPage extends Page {
    data: Array<Round>;
}

class RosterPage extends Page {
    data: Array<Roster>;
}

class OrganizationPage extends Page {
    data: Array<Organization>;
}

/* A BasicArticle is basically an article */
class BasicArticle {
    id: Number;

    /**
     * The author of the article.
     * @see https://siege.gg/user/<author>
     */
    author: String;

    /** The article language */
    language: ArticleLanguage;

    /** The article title */
    title: String;

    /** The article hook */
    hook: String;

    /** Up to 3 tags of the article */
    tags: Array<String>;

    /** When the article was created */
    created_at: Date;
}

class BasicRequester {
    constructor(client: SiegeGGApiClient);

    news(): Promise<BasicArticlePage>;
    blog(): Promise<BasicArticlePage>;
}

class Season {
    id: Number;

    /** The name of the season */
    name: String;

    /** The region of the season (may be INTL for international seasons or majors). */
    region: String;

    /** `match_count` is the amount of the matches in this season. */
    match_count: Number;
}

class Match {
    id: Number;

    /** A list of games (i.e. maps played) */
    games: Array<Game>;

    /** Both rosters  */
    rosters: Array<Roster>;

    /** `match_count` is the amount of the matches in this season. */
    match_count: Number;
}

class Player {
    /** The in-game name of the player */
    ign: String;

    /** The name of the player */
    name: String;

    /** The nationality of the player */
    nationality: String;

    /** The roster id of the player */
    roster_id: Number;

    /** The last update of the player‘s data */
    updated_at: Date;
}

class OperatorPick {
    stats_round_id: Number;

    operator: Operator;

    six_pick: Number;

    replaced_operator: Operator;
}

enum OperatorSide {
    ATTACK = 'ATTACK',
    DEFEND = 'DEFEND',
}

class Operator {
    name: String;
    side: OperatorSide;
}

class OperatorBan {
    stats_roster_id: Number;
    operator: Operator;
}

class Game {
    id: Number;

    win_roster_id: Number;
    loss_roster_id: Number;

    win_score: Number;
    loss_score: Number;

    operator_bans: Array<OperatorBan>;
}

enum PlantSide {
    A = 'A',
    B = 'B',
}

class Round {
    id: Number;

    /** The ID of the parent game the round was played in */
    stats_game_id: Number;

    /** The site which was selected by the defenders */
    site: String;

    /** The roster id of the defending team */
    def_roster_id: Number;

    /** The roster id of the attacking team */
    atk_roster_id: Number;

    /** Whether the defenders won (true) or the attackers won (false) */
    def_win: Boolean;

    /** Whether the defuser was planted */
    was_plant: Boolean;

    /** The name of the planting player, if `was_plant` is true */
    plant_player_ign?: String;

    /** The time when the defuser was planted, if `was_plant` is true */
    plant_time?: String;

    /** The site (A or B) where was planted, if `was_plant` is true */
    plant_site?: PlantSide;

    /** Whether the defuser, if `was_plant` is true, was disabled by the defenders */
    was_disabled?: Boolean;

    /** The name of the player who disabled the defuser, if `disable_player_ign` is true */
    disable_player_ign?: String;

    /** The condition of the win */
    win_condition: String;

    /** The length of the round */
    round_length: String;
}

class Roster {
    id: Number;

    /** The name of the roster */
    name: String;

    /** The id of the roster‘s current organization */
    current_org_id?: Number;

    /** The start date when the roster became active */
    start_date: Date;

    /** The end date when the roster became inactive. This field only has a date when `is_active` is false. */
    end_date?: Date;

    is_active: Boolean;
}

class Organization {
    /** The name of the organization */
    name: String;

    /** A short tag of the organization */
    tag?: String;

    /** The region of the organization */
    region: String;

    /** The flag region code of the organization. This most likely differs from `region`! */
    flag: String;

    /** The brand color, or a similar color, of the organization. */
    color: String;

    /** The brand logo, or a similar logo, of the organization. **Warning**: the url may result in a HTTP 404. */
    logo_url?: URL;
}
