const fetch = require('node-fetch');

class SiegeGGApiClient {
    constructor(options) {
        this.options = options;
        if (!this.options.version) this.options.version = 'v1';

        this.defaultHeaders = {
            'User-Agent':
                'siegegg-api/1.0 (+https://gitlab.com/SiegeGG/API/SiegeGG-Node.js)',
            Authorization: this.options.token,
        };
    }

    page(page = 0) {
        this.page = page;
        return this;
    }

    app() {
        return this.makeRequest(`app`, ApiApplication);
    }

    updateAppTitle(title) {
        return new Promise((resolve, reject) => {
            fetch(`https://api.siege.gg/${this.options.version}/app`, {
                headers: this.defaultHeaders,
                method: 'POST',
                body: new URLSearchParams([['title', title]]),
            })
                .then(res => res.json())
                .then(json => {
                    if (json.error) reject(JSON.stringify(json));
                    else resolve(new ApiApplication(json));
                })
                .catch(reject);
        });
    }

    basic() {
        return new BasicRequester(this);
    }

    seasons() {
        return new Promise((resolve, reject) => {
            fetch(
                `https://api.siege.gg/${this.options.version}/seasons?page=${
                    this.page
                }`,
                { headers: this.defaultHeaders }
            )
                .then(res => res.json())
                .then(json => {
                    if (json.error) reject(JSON.stringify(json));
                    else resolve(new Seasons(json));
                })
                .catch(reject);
        });
    }

    matches() {
        return new Promise((resolve, reject) => {
            fetch(
                `https://api.siege.gg/${this.options.version}/matches?page=${
                    this.page
                }`,
                { headers: this.defaultHeaders }
            )
                .then(res => res.json())
                .then(json => {
                    if (json.error) reject(JSON.stringify(json));
                    else resolve(new Matches(json));
                })
                .catch(reject);
        });
    }

    match(id) {
        return this.makeRequest(`matches/${id}`, Match);
    }

    players() {
        return this.makeRequest(`players`, Players);
    }

    player(ign) {
        return this.makeRequest(`players/${ign}`, Player);
    }

    playerPicks(ign) {
        return this.makeRequest(`players/${ign}/picks`, PlayerPicks);
    }

    games() {
        return this.makeRequest(`games`, Games);
    }

    game(id) {
        return this.makeRequest(`games/${id}`, Game);
    }

    rounds() {
        return this.makeRequest(`rounds`, Rounds);
    }

    round(id) {
        return this.makeRequest(`rounds/${id}`, Round);
    }

    rosters() {
        return this.makeRequest(`rosters`, Rosters);
    }

    roster(id) {
        return this.makeRequest(`rosters/${id}`, Roster);
    }

    orgs() {
        return this.makeRequest(`orgs`, Organizations);
    }

    org(id) {
        return this.makeRequest(`orgs/${id}`, Organization);
    }

    makeRequest(endpoint, type) {
        return new Promise((resolve, reject) => {
            fetch(`https://api.siege.gg/${this.options.version}/${endpoint}`, {
                headers: this.defaultHeaders,
            })
                .then(res => res.json())
                .then(json => {
                    if (json.error) reject(JSON.stringify(json));
                    else resolve(new type(json));
                })
                .catch(reject);
        });
    }
}

class ApiClientOptions {}

class BasicArticlePage {
    constructor(json) {
        this.data = json;
        this.pagination = null;
    }
}

class BasicRequester {
    constructor(client) {
        this.client = client;
    }

    news(blog = false) {
        return new Promise((resolve, reject) => {
            fetch(
                `https://api.siege.gg/${this.client.options.version}/basic/${
                    blog ? 'blog' : 'news'
                }`,
                { headers: this.client.defaultHeaders }
            )
                .then(res => res.json())
                .then(json => {
                    if (json.error) reject(JSON.stringify(json));
                    else resolve(new BasicArticlePage(json));
                })
                .catch(reject);
        });
    }

    blog() {
        return this.news(true);
    }
}

class SimpleResponse {
    constructor(response) {
        if (response.data) {
            for (var key in response.data) {
                this[key] = response.data[key];
            }
        } else {
            for (var key in response) {
                this[key] = response[key];
            }
        }

        this.recursiveDate(this);
    }

    recursiveDate(data) {
        for (var key in data) {
            if (typeof data[key] == 'object') {
                this.recursiveDate(data[key]);
                continue;
            }
            if (
                key == 'quota_reset' ||
                key == 'created_at' ||
                key == 'updated_at' ||
                key == 'date' ||
                key == 'start_date' ||
                key == 'end_date'
            )
                data[key] = new Date(data[key]);
            else data[key] = data[key];

            if (key == 'logo_url') data[key] = new URL(data[key]);
        }
    }
}

class PaginatedResponse {
    constructor(data) {
        this.data = data.data;
        this.pagination = data.pagination;

        this.recursiveDate(this.data);
    }

    recursiveDate(data) {
        for (var attr in data) {
            if (typeof data[attr] == 'object') {
                this.recursiveDate(data[attr]);
                continue;
            }
            if (
                attr == 'quota_reset' ||
                attr == 'created_at' ||
                attr == 'updated_at' ||
                attr == 'date' ||
                attr == 'start_date' ||
                attr == 'end_date'
            )
                data[attr] = new Date(data[attr]);

            if (attr == 'logo_url') data[attr] = new URL(data[attr]);
        }
    }
}

class ApiApplication extends SimpleResponse {}

class Seasons extends PaginatedResponse {}

class Matches extends PaginatedResponse {}
class Match extends SimpleResponse {}

class Players extends PaginatedResponse {}
class Player extends SimpleResponse {}
class PlayerPicks extends SimpleResponse {}

class Games extends PaginatedResponse {}
class Game extends SimpleResponse {}

class Rounds extends PaginatedResponse {}
class Round extends SimpleResponse {}

class Rosters extends PaginatedResponse {}
class Roster extends SimpleResponse {}

class Organizations extends PaginatedResponse {}
class Organization extends SimpleResponse {}

module.exports = {
    SiegeGGApiClient,
};
