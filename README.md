# SiegeGG API

This package is a small library built for interation with the [SiegeGG](https://siege.gg/) API. The package features full support for community and enterprise features. You can find details on the API [here](https://dev.siege.gg/docs/1.0/preamble).

## Usage

Before you start using this library, you need to [register a SiegeGG account](https://siege.gg/register), if you have not already one, and then create an API application at the [dev hub](http://dev.siege.gg/). There you will obtain an API token that authorizes your application.

```js
const { SiegeGGApiClient } = require('siegegg-api');

const api = new SiegeGGApiClient({
    // replace a_random_token with your obtained API token
    token: 'a_random_token',
});

// using Promises

api.player('PENGU').then(player => console.log(player.nationality)); // Denmark

api.basic()
    .news()
    .then(articles => {
        let authors = articles.data.map(article => article.author);
        console.log(authors); // [TheRussianEwok, Boxi, ChankaNewsNetwork, ...]
    });

// or using async/await

let player = await app.player('PENGU');
console.log(player.nationality); // Denmark

let articles = await app.basic().news();
let authors = articles.data.map(article => article.author);
console.log(authors); // [TheRussianEwok, Boxi, ChankaNewsNetwork, ...]
```

## License

AGPL-v3.0
